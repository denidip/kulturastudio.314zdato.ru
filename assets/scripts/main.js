/**
 * Created by denidip on 27.10.16.
 */
var isTouchDevice = navigator.userAgent.match(/(iPhone|iPod|iPad|Android|BlackBerry|Windows Phone)/);

$(document).ready(function() {

    var marker = true;
    var sectionsLength = $("main .section").length - 1;

    $('article.wrapper').fullpage({
        loopBottom: true,
        autoScrolling: true,
        scrollOverflow: true,
        scrollOverflowOptions: {
            click: false,
            tap: true
        },
        normalScrollElementTouchThreshold: 7,
        normalScrollElements: '.onscroll',

        onLeave: function(index, nextIndex, direction) {
            if ( index === sectionsLength && direction === 'down') {
                function count() {
                    if(!isTouchDevice){
                        if(!state_menu){
                            slider.trigger('next.owl.carousel');
                            marker = false;
                        }
                    }
                }
                function resetmarker() {
                    marker = true
                }
                if ( marker == true ) {
                    count();
                    setTimeout(resetmarker, 1500);
                }
                return false;
            }
            if( nextIndex === sectionsLength ) {
                if(!state_menu){
                    $('header').addClass('inverse');
                    //console.log('FUCK!!!!!');
                }
            }
            if(index > 0){
                if(state_menu){
                    return false;
                }
            }
            if( nextIndex != sectionsLength ) {
                $('header').removeClass('inverse');
                //console.log('GOOD');
            }
        }
    });

    var slider = $('#works').owlCarousel({
        items: 1,
        loop: true,
        smartSpeed: 1200,
        dots: true,
        mouseDrag: false,
        responsive: {
            0: {
                nav: false
            },
            1000: {
                nav: true
            }
        }
    });

    $('.gallery a.img').on('click', function(){
        $('#modalgallery').foundation('open');
        //iscrollReveal.refresh();
    });

});
