$(document).ready( function(){
    var toggle_menu_btn = $('button#toggle-menu');
    var toggle_menu = $('div.toggle-menu-panel');
    var toggle_overlay = $('div.toggle-menu-overlay');
    var wrapper = $('main');
    var header = $('header');
    var toggle_menu_height = $("div.toggle-menu-panel .align-self-top").outerHeight(true) + $("div.toggle-menu-panel .align-self-bottom").outerHeight(true);

    state_menu = false;

    function togglemenu() {
        if(state_menu == false) {
            state_menu = true;
            toggle_menu_btn.addClass('active');
            wrapper.addClass('slide_left');
            //$.fn.fullpage.setAllowScrolling(false);
            toggle_overlay.addClass('active');
            toggle_menu.addClass('active');
            header.addClass('inverse2');
            //console.log('Toggle-menu is open');
        }else{
            state_menu = false;
            toggle_menu_btn.removeClass('active');
            wrapper.removeClass('slide_left');
            //$.fn.fullpage.setAllowScrolling(true);
            toggle_overlay.removeClass('active');
            toggle_menu.removeClass('active');
            header.removeClass('inverse2');
            //console.log('Toggle-menu is close');
        }
    }

    toggle_menu_btn.on('click', function(){ togglemenu() });
    toggle_overlay.on('click', function(){ togglemenu() });

    $(document).ready(autoHeight);
    $(window).resize(autoHeight);

    function autoHeight() {
        if(toggle_menu_height >= toggle_overlay.height()){
            $('.fullheight').addClass('false');
        }else{
            $('.fullheight').removeClass('false');
        }
        iscrollMenu.refresh();
        //console.log('window = ' + toggle_overlay.height());
    }
});
iscrollMenu = new IScroll('#iscrollMenu',{
    mouseWheel: true,
    scrollbars: true,
    click: true
});
iscrollMain = new IScroll('.iscrollMain',{
    mouseWheel: true,
    scrollbars: true,
    preventDefault: false,
    tap: true
});

$(document).foundation();